
import React, { Component } from "react";
import { useLocation } from "react-router-dom";
import { Navbar, Container, Nav, Dropdown, Button } from "react-bootstrap";

import routes from "../routes.js";


class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
        isLoading: false,
        categories: []
    };
  }


  render() {
  return (
    <header className="header">
        <div className="container">
            <div className="row">
                <div className="col-lg-2">
                    <div className="header__logo">
                        <a href="./index.html">
                            Logo
                        </a>
                    </div>
                </div>
                <div className="col-lg-8">
                    <div className="header__nav">
                        <nav className="header__menu mobile-menu">
                     
                            <ul>
                                <li className="active"><a href="./index.html">Home</a></li>
                                <li><a href="./categories.html">Categories <span className="arrow_carrot-down"></span></a>
                                </li>
                                <li><a href="/login">Login</a></li>
                                <li><a href="#">Contacts</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="col-lg-2">
                    <div className="header__right">
                        <a href="#" className="search-switch"><i className="fa fa-search" /></a>
                        <a href="./login.html"><i className="fa fa-user" /></a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
  );
  }
}

export default Header;
