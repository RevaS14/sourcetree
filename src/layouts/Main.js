
import React, { Component } from "react";
import { useLocation, Route, Routes } from "react-router-dom";

import Header from "../components/Header";
import Footer from "../components/Footer";

import routes from "../routes.js";

function Main() {
  const getRoutes = (routes) => {
    console.log(routes);
    return routes.map((prop, key) => {
      if (prop.layout === "/main") {
        return (
          <Route exact path={prop.path} element={< prop.component />} key={key}></Route>
        );
      } else {
        return null;
      }
    });
  };

  return (
    <>
      <div className="wrapper">
      
        <div className="main-panel">
          <Header />
          <div className="content">
            <Routes>{getRoutes(routes)}
            
            </Routes>
          </div>
          <Footer />
         
        </div>
      </div>
     
    </>
  );
}

export default Main;
