
import Home from "./views/Home.js";
import Login from "./views/Login.js";

const dashboardRoutes = [
  {
    path: "/",
    name: "Home",
    icon: "nc-icon nc-chart-pie-35",
    component: Home,
    layout: "/main",
  },
  {
    path: "/login",
    name: "Login",
    icon: "nc-icon nc-chart-pie-35",
    component: Login,
    layout: "/main",
  }
];

export default dashboardRoutes;
